package com.example;

import com.example.grpc.GreetingServiceOuterClass;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;

public class Service extends com.example.grpc.GreetingServiceGrpc.GreetingServiceImplBase {
    public static void main(String[] args) {
        Server server = ServerBuilder
                .forPort(8090)
                .addService(new Service())
                .build();

        startServer(server);
        awaitTerminationServer(server);
    }

    private static void startServer(Server server) {
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void awaitTerminationServer(Server server) {
        try {
            server.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void greeting(GreetingServiceOuterClass.HelloRequest request,
                         StreamObserver<GreetingServiceOuterClass.HelloResponse> responseObserver) {

        for (int i = 0; i < 5; i++) {

            System.out.println(request);

            GreetingServiceOuterClass.HelloResponse response = GreetingServiceOuterClass.HelloResponse
                    .newBuilder().setGreeting("Hello from Server, " + request.getName())
                    .build();

            responseObserver.onNext(response);
        }

        responseObserver.onCompleted();
    }
}